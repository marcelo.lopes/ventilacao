#include <DS18B20.h>
#include "Relay.h"

DS18B20 ds(8);

Relay light(2, false); // constructor receives (pin, isNormallyOpen)


void setup() {
  //set port to display mensagem
  Serial.begin(9600);

  light.begin(); // inicializes the pin

}

void loop() {
  // ler a temperatura do sensor

  // se estiver mais q 30C ligar ventuinha


    // Serial.println("turnOff");
    // light.turnOff(); //turns relay off	
    // delay(5000);
    // Serial.println("turnOn");
    // light.turnOn();  
    // delay(5000);

  // se estiver menos q 30C desligar 

  while (ds.selectNext()) {
    Serial.println(ds.getTempC());
    if (ds.getTempC() > 30.0) {
      light.turnOn(); 
    } else {
      light.turnOff();
    }
    delay(10000);
  }  
}
